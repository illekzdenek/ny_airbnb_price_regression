import logging
from sklearn.metrics import mean_absolute_error
from tensorflow.keras import regularizers
import math
import pickle
from matplotlib import pyplot as plt
from sklearn.tree import DecisionTreeRegressor
from sklearn.metrics import r2_score
from sklearn.metrics import classification_report
from sklearn import metrics
import warnings
import lime.lime_tabular
import lime
import eli5
from sklearn.utils import resample
from sklearn.kernel_ridge import KernelRidge
from sklearn.svm import SVR
from sklearn.ensemble import RandomForestRegressor, GradientBoostingRegressor
from sklearn.gaussian_process import GaussianProcessRegressor
from xgboost import XGBRegressor
from sklearn.metrics import mean_squared_error, r2_score
from sklearn.linear_model import Ridge, RidgeCV, Lasso, LassoCV, LinearRegression, ElasticNet,  HuberRegressor
from sklearn.model_selection import train_test_split, cross_val_score, GridSearchCV, KFold, StratifiedKFold, RandomizedSearchCV
from sklearn.compose import ColumnTransformer
from sklearn.pipeline import Pipeline
from sklearn.preprocessing import scale, StandardScaler, RobustScaler, OneHotEncoder, PolynomialFeatures
import statsmodels.api as sm
import statsmodels
from scipy.special import boxcox1p
from scipy.stats import norm
import scipy.stats as stats
import itertools
import collections
from plotly.subplots import make_subplots
import plotly.graph_objs as go
import plotly as py
from plotly.offline import download_plotlyjs, init_notebook_mode, plot, iplot
import numpy as np  # linear algebra
import pandas as pd  # data processing, CSV file I/O (e.g. pd.read_csv)
import matplotlib
import matplotlib.pyplot as plt
import seaborn as sns
from keras.models import Sequential
from keras.layers import Dense
from keras.wrappers.scikit_learn import KerasRegressor
from sklearn.model_selection import cross_val_score
from sklearn.model_selection import KFold
from sklearn.preprocessing import StandardScaler
from sklearn.pipeline import Pipeline
plt.style.use('fivethirtyeight')
matplotlib.rcParams['font.family'] = "Arial"



logging.basicConfig(filename="history.log",
                    format="%(asctime)s:%(levelname)s:%(message)s")

#print(statsmodels.__version__)


#Model interpretation modules
#import shap
#shap.initjs()

warnings.filterwarnings("ignore", category=FutureWarning)

#Load Data
Combined_data = pd.read_csv('../AB_NYC_2019.csv')
Combined_data.head()
print('Number of features: {}'.format(Combined_data.shape[1]))
print('Number of examples: {}'.format(Combined_data.shape[0]))
Combined_data.dtypes
Combined_data['last_review'] = pd.to_datetime(Combined_data['last_review'], infer_datetime_format=True)

#Missing data
#total = Combined_data.isnull().sum().sort_values(ascending=False)
#percent = (Combined_data.isnull().sum()) / \
#    Combined_data.isnull().count().sort_values(ascending=False)
#missing_data = pd.concat([total, percent], axis=1, keys=['Total', 'Percent'], sort=False).sort_values('Total', ascending=False)
#missing_data.head(40)
#
#Combined_data[Combined_data['number_of_reviews'] == 0.0].shape
#Combined_data['reviews_per_month'] = Combined_data['reviews_per_month'].fillna(
#    0)
#earliest = min(Combined_data['last_review'])
#Combined_data['last_review'] = Combined_data['last_review'].fillna(earliest)
#Combined_data['last_review'] = Combined_data['last_review'].apply(
#    lambda x: x.toordinal() - earliest.toordinal())
#total = Combined_data.isnull().sum().sort_values(ascending=False)
#percent = (Combined_data.isnull().sum()) / \
#    Combined_data.isnull().count().sort_values(ascending=False)
#missing_data = pd.concat([total, percent], axis=1, keys=['Total', 'Percent'], sort=False).sort_values('Total', ascending=False)
#missing_data.head(40)


Combined_data.drop(['host_name', 'name', 'last_review', 'reviews_per_month', 'calculated_host_listings_count'], axis=1, inplace=True)
Combined_data.dropna()
Combined_data = Combined_data[Combined_data['price'] <= 1000]
Combined_data = Combined_data[Combined_data['price'] >= 1]
Combined_data = Combined_data[Combined_data['minimum_nights'] <= 7]
Combined_data = Combined_data[Combined_data['minimum_nights'] > 0]

##Price distribution
#
#fig, axes = plt.subplots(1, 3, figsize=(21, 6))
#sns.distplot(Combined_data['price'], ax=axes[0])
#sns.distplot(np.log1p(Combined_data['price']), ax=axes[1])
#axes[1].set_xlabel('log(1+price)')
#sm.qqplot(np.log1p(Combined_data['price']),stats.norm, fit=True, line='45', ax=axes[2])
##Combined_data = Combined_data[np.log1p(Combined_data['price']) < 8]
##Combined_data = Combined_data[np.log1p(Combined_data['price']) > 3]
#fig, axes = plt.subplots(1, 3, figsize=(21, 6))
#sns.distplot(Combined_data['price'], ax=axes[0])
#sns.distplot(np.log1p(Combined_data['price']), ax=axes[1])
#axes[1].set_xlabel('log(1+price)')
#sm.qqplot(np.log1p(Combined_data['price']),stats.norm, fit=True, line='45', ax=axes[2])
#Combined_data['price'] = np.log1p(Combined_data['price'])

#Predictor distributions
print(Combined_data.columns)
print('In this dataset there are {} unique hosts renting out  a total number of {} properties.'.format(
    len(Combined_data['host_id'].unique()), Combined_data.shape[0]))
Combined_data = Combined_data.drop(['host_id', 'id'], axis=1)

#neighbourhood group
sns.catplot(x='neighbourhood_group', kind='count', data=Combined_data)
fig = plt.gcf()
fig.set_size_inches(12, 6)


#Longitude and Latitude
fig, axes = plt.subplots(1, 3, figsize=(21, 6))
sns.distplot(Combined_data['latitude'], ax=axes[0])
sns.distplot(Combined_data['longitude'], ax=axes[1])
sns.scatterplot(x=Combined_data['latitude'], y=Combined_data['longitude'])

#Room type
sns.catplot(x='room_type', kind='count', data=Combined_data)
fig = plt.gcf()
fig.set_size_inches(8, 6)

#Minimum nights
#fig, axes = plt.subplots(1, 2, figsize=(21, 6))
#
#sns.distplot(Combined_data['minimum_nights'],rug=False, kde=False, color="green", ax=axes[0])
#axes[0].set_yscale('log')
#axes[0].set_xlabel('minimum stay [nights]')
#axes[0].set_ylabel('count')
#
#sns.distplot(np.log1p(Combined_data['minimum_nights']),rug=False, kde=False, color="green", ax=axes[1])
#axes[1].set_yscale('log')
#axes[1].set_xlabel('minimum stay [nights]')
#axes[1].set_ylabel('count')

#Reviews per month
#fig, axes = plt.subplots(1, 2, figsize=(18.5, 6))
#sns.distplot(Combined_data[Combined_data['reviews_per_month'] < 17.5]['reviews_per_month'], rug=True, kde=False, color="green", ax=axes[0])
#sns.distplot(np.sqrt(Combined_data[Combined_data['reviews_per_month'] < 17.5]['reviews_per_month']), rug=True, kde=False, color="green", ax=axes[1])
#axes[1].set_xlabel('ln(reviews_per_month)')
#fig, axes = plt.subplots(1, 1, figsize=(21, 6))
#sns.scatterplot(x=Combined_data['availability_365'],y=Combined_data['reviews_per_month'])
#Combined_data['reviews_per_month'] = Combined_data[Combined_data['reviews_per_month']< 17.5]['reviews_per_month']

#Availability 365
fig, axes = plt.subplots(1, 1, figsize=(18.5, 6))
sns.distplot(Combined_data['availability_365'],rug=False, kde=False, color="blue", ax=axes)
axes.set_xlabel('availability_365')
axes.set_xlim(0, 365)


#Feature engineering
#Combined_data['all_year_avail'] = Combined_data['availability_365'] > 353
#Combined_data['low_avail'] = Combined_data['availability_365'] < 12
#Combined_data['no_reviews'] = Combined_data['reviews_per_month'] == 0

#pearson corelation matrix
corrmatrix = Combined_data.corr(method='pearson')#corrmatrix = Combined_data.corr()
f, ax = plt.subplots(figsize=(15, 12))
sns.heatmap(corrmatrix, vmax=0.8, square=True)
sns.set(font_scale=0.8)


print(Combined_data.head())

#Correlation features selection
#olumns = np.full((corrmatrix.shape[0],), True, dtype=bool)
#or i in range(corrmatrix.shape[0]):
#   for j in range(i+1, corrmatrix.shape[0]):
#       print('>=')
#       print(corrmatrix.iloc[i, j] >= 0.5)
#       print('<=')
#       print(corrmatrix.iloc[i, j] <= -0.5)
#       #print(corrmatrix.iloc[i, j])
#       
plt.tight_layout()
plt.show()

#Encoding categorical features One Hot Encoding
categorical_features = Combined_data.select_dtypes(include=['object']) #najde datatypy v dataframu, kteří jsou typu objekt
print('Categorical features: {}'.format(categorical_features.shape))
categorical_features_one_hot = pd.get_dummies(categorical_features)# automaticky vytvoří kategorický data (např. část města) na číselné hodnoty
categorical_features_one_hot.head()
#Combined_data['reviews_per_month'] = Combined_data['reviews_per_month'].fillna(0)

#Encoding categorical features Label Encoding
neighbourhood_group = list(set(categorical_features['neighbourhood_group']))
neighbourhood = list(set(categorical_features['neighbourhood']))
room_type = list(set(categorical_features['room_type']))
#print(neighbourhood_group)
#print(len(neighbourhood_group))
#print(neighbourhood)
#print(len(neighbourhood))
#print(room_type)
#print(len(room_type))
nghb_g = {index: i for index,i in enumerate(neighbourhood_group)}
#print(nghb_g)
nghb = {index: i for index, i in enumerate(neighbourhood)}
#print(nghb)
rmtp = {index: i for index, i in enumerate(room_type)}
#print(rmtp)


nghb_g_codes, nghb_g_uniques = pd.factorize(categorical_features['neighbourhood_group'])
nghb_codes, nghb_uniques = pd.factorize(categorical_features['neighbourhood'])
rmtp_codes, rmtp_uniques = pd.factorize(categorical_features['room_type'])

factorized_features = pd.DataFrame()
factorized_features['neighbourhood_group'] = pd.Series(nghb_g_codes)
factorized_features['neighbourhood'] = pd.Series(nghb_codes)
factorized_features['room_type'] = pd.Series(rmtp_codes)

print(factorized_features.head())
print(categorical_features_one_hot.head())

print('Categorical features: ')
print(categorical_features.columns)


#Save transformed dataframe for future use
# vyberou se z původního datasetu číselné hodnoty
numerical_features = Combined_data.select_dtypes(exclude=['object'])
print(Combined_data.dtypes)
y = numerical_features.price # price jako target
numerical_features = numerical_features.drop(['price'], axis=1) #vyloučení price z features
print('Numerical features: {}'.format(numerical_features.shape))
#vykreslit grafy v závislosti s cenou
print('tady pdo timto bude y!!')
print(y)
for f in numerical_features.columns:
    plt.xlabel(f)
    plt.ylabel("price")
    plt.scatter(numerical_features[f], y)
    #plt.subplots_adjust(bottom=0.102, left=0.120)
    plt.tight_layout()
    plt.savefig(f+'_feature.png')
    plt.show()

numerical_features.reset_index(drop=True, inplace=True)
factorized_features.reset_index(drop=True, inplace=True)


X = np.concatenate((numerical_features, factorized_features), axis=1) # factorized_features <---> categorical_features_one_hot # zkombinování původních číselných dat a kategorických převedených dat
X_df = pd.concat([numerical_features, factorized_features], axis=1 )

print([len(X_df[i]) for i in X_df.columns])
print(len(y))
print([len(Combined_data[i]) for i in Combined_data.columns])
print([len(factorized_features[i]) for i in factorized_features.columns])
print([len(numerical_features[i]) for i in numerical_features.columns])
print(X_df.head())

#remove duplicates
print('--------------------------')
print(X_df.describe())
#print('Dimensions of the design matrix: {}'.format(X.shape))
#print('Dimension of the target vector: {}'.format(y.shape))



#Train-test split
X_train, X_test, y_train, y_test = train_test_split(
    X_df, y, test_size=0.2, random_state=42)
print('Dimensions of the training feature matrix: {}'.format(X_train.shape))
print('Dimensions of the training target vector: {}'.format(y_train.shape))
print('Dimensions of the test feature matrix: {}'.format(X_test.shape))
print('Dimensions of the test target vector: {}'.format(y_test.shape))

#Fitting the Multiple Linear Regression model
mlr = LinearRegression()
mlr.fit(X_train, y_train) 

# Intercept and Coefficient
print("Intercept: ", mlr.intercept_)
print("Coefficients:")
print(list(zip(X_df, mlr.coef_)))


#Prediction of test set
y_pred_mlr = mlr.predict(X_test)
y_test = y_test
y_pred_mlr = y_pred_mlr
#Predicted values
print("Prediction for test set: {}".format(y_pred_mlr))
#Actual value and the predicted value
mlr_diff = pd.DataFrame(
    {'Actual value': y_test, 'Predicted value': y_pred_mlr})
print(mlr_diff.head(40))

#Model Evaluation
r2score = r2_score(y_test, y_pred_mlr)
meanAbErr = metrics.mean_absolute_error(y_test, y_pred_mlr)
meanSqErr = metrics.mean_squared_error(y_test, y_pred_mlr)
maxErr = metrics.max_error(y_test, y_pred_mlr)
rootMeanSqErr = np.sqrt(metrics.mean_squared_error(y_test, y_pred_mlr))
print('R2 score:', r2score)
#print('R squared: {:.2f}'.format(mlr.score(X_df, y)*100))
print('Mean Absolute Error:', meanAbErr)
print('Mean Square Error:', meanSqErr)
print('Root Mean Square Error:', rootMeanSqErr)
print('Max Error:', maxErr)
print('-------------------------------------------')
#-------------------------NN-+-------------------------
# define base model
#def baseline_model():
#	# create model
#	model = Sequential()
#	model.add(Dense(241, input_dim=241, kernel_initializer='normal', activation='relu')) #přidávání skrytých, vstupních a výstupních vrstev | relu je udajně nejlepší, ale další je třeba sigmoid - asi vyzkoušet
#	model.add(Dense(1, kernel_initializer='normal'))
#	# Compile model
#	model.compile(loss='mean_squared_error', optimizer='adam')
#	return model


# train first NN

#poly = PolynomialFeatures(1)
#ypoly = y_train
#xpoly = poly.fit_transform(X_train)
#ypoly = np.asarray(ypoly).astype(np.float32)
#xpoly = np.asarray(xpoly).astype(np.float32)
#model = baseline_model()
#history = model.fit(xpoly,ypoly,epochs=5,batch_size=10,validation_split= 0.3)
#
#
#plt.plot(history.history['loss'], label='train loss')
#plt.plot(history.history['val_loss'], label='validation loss')
#plt.ylabel('Loss')
#plt.xlabel('Number of epochs')
#plt.title('Learning curve - n. epochs', fontsize=18, y=1.03)
#plt.legend()
#plt.savefig('NN_learning curve.png')
#plt.show()

def FindLayerNodesLinear(n_layers, last_layer_nodes, first_layer_nodes):
    layers = []

    nodes_increment = (last_layer_nodes - first_layer_nodes) / (n_layers-1)
    nodes = first_layer_nodes
    for i in range(1, n_layers+1):
        layers.append(math.ceil(nodes))
        nodes = nodes + nodes_increment

    return layers


# loss_func='binary_crossentropy'
def createmodel(last_layer_nodes=3, n_layers=3, activation_func="relu", loss_func='mse', l=0.001, n_inputs=45):
    model = Sequential()
    n_nodes = FindLayerNodesLinear(n_layers, last_layer_nodes, n_inputs)
    for i in range(1, n_layers):
        if i==1:
            model.add(Dense(45, input_dim=n_inputs, activation=activation_func,
                            activity_regularizer=regularizers.l2(l)))
        else:
            model.add(Dense(n_nodes[i-1], activation=activation_func,
                            activity_regularizer=regularizers.l2(l)))
            
    #Finally, the output layer should have a single node in binary classification
    model.add(Dense(1, activation=activation_func))
    model.compile(optimizer='adam', loss=loss_func, metrics = ["accuracy"]) #note: metrics could also be 'mse'
    
    return model




#train optimized # zakomentovat model, history a níže pro hledání optimálních parametrů a odkomentovat ######## a níže
poly = PolynomialFeatures(2)
ypoly = y_train
xpoly = poly.fit_transform(X_train)
model = createmodel()
print(xpoly)
print(ypoly)


logging.warning('Trénink')

## trénink ##
history = model.fit(xpoly, ypoly, epochs=20,batch_size=20, validation_split=0.3)
plt.plot(history.history['loss'], label='train loss')
plt.plot(history.history['val_loss'], label='validation loss')
plt.ylabel('MSE loss')
plt.xlabel('Number of epochs')
plt.title('Learning curve - n. epochs', fontsize=18, y=1.03)
plt.legend()
plt.savefig('NN_learning curve.png')
plt.tight_layout()
plt.show()

x_test_poly = poly.transform(X_test)
yPredict = model.predict(x_test_poly)

y_test = pd.DataFrame(y_test)

prediction = y_test.copy()
prediction['pricePredicted'] = yPredict
prediction['difference'] = abs(y_test-yPredict)
results = prediction.copy()
for col in Combined_data.columns:
    results[col] = Combined_data[col]
results.to_excel('vysledky.xlsx')

big_difference = prediction[prediction.difference >
                            mean_absolute_error(y_test, yPredict)]
okey = prediction[prediction.difference <
                  mean_absolute_error(y_test, yPredict)]
ranges = range(0, 50000, 2000)
distribution = prediction.groupby(
    pd.cut(prediction.difference, ranges)).count()
mistake_distribution = big_difference.groupby(
    pd.cut(big_difference.difference, ranges)).count()
mean = mistake_distribution/distribution
print(mean)

print(prediction['difference'].median())
print(okey['difference'].median())

logging.warning("Mean absolute error: {}".format(mean_absolute_error(y_test, yPredict)))
logging.warning("Mean squared error: {}".format(mean_squared_error(y_test, yPredict)))
print("Mean absolute error: {}".format(mean_absolute_error(y_test, yPredict)))
print("Mean squared error: {}".format(mean_squared_error(y_test, yPredict)))

######## Hledání nejlepších parametrů ########

#logging.warning('Hledání optimálních parametrů')
#
###logging.warning("This is warning")
###logging.error("This is Error")
###logging.critical("This is Critical")
#regressor = KerasRegressor(build_fn=createmodel, verbose=False)
#print(regressor.get_params())
#
##nejlepší polynom
#ypoly = y[:1000]
#loss = []
#val_loss = []
#for i in range(1, 4):
#    poly = PolynomialFeatures(i)
#    xpoly = poly.fit_transform(X[:1000])
#    print(poly.n_output_features_)
#    model = createmodel(n_inputs=poly.n_output_features_)
#    history = model.fit(xpoly, ypoly, epochs=20,
#                        batch_size=10, validation_split=0.33)
#    loss.append(history.history['loss'][-1])
#    val_loss.append(history.history['val_loss'][-1])
#
#
#plt.plot(range(1, 4), loss, label='Loss')
#plt.plot(range(1, 4), val_loss, label='Validation loss')
#plt.legend()
#plt.ylabel('MSE', fontsize=14)
#plt.xlabel('Polynominal degress', fontsize=14)
#plt.title('Validation curve - polynominal degree', fontsize=18, y=1.03)
#plt.legend()
#plt.tight_layout()
#plt.show()
#plt.savefig('polynominal_degree.png')
#
#poly = PolynomialFeatures(2)
#ypoly = y_train
#xpoly = poly.fit_transform(X_train)
#model = createmodel()
#param_grid = {'batch_size': [100, 20, 50, 25, 32],
#              'nb_epoch': [200, 100, 300, 400]
#              }
### hledání nejlepší lambdy
#l = [0, 0.001, 0.01, 0.1, 1, 10, 100]
#param_grid = dict(
#    #n_layers=[2, 3, 4, 5], # počet vrstev
#    #first_layer_nodes=[240, 241], 
#    #last_layer_nodes=[4],  #počet neuronů na poseldní vrstvě
#    #activation_func=activation_funcs, #aktivační funkce
#    #loss_func=loss_funcs, 
#    #batch_size=[100, 200, 500, 1000, 2000, 5000], # 
#    #nb_epoch=[20, 10, 30, 40, 50],
#    l=l,
#    #n_inputs=[poly.n_output_features_]
#)
#
#grid = GridSearchCV(estimator=regressor, param_grid=param_grid, n_jobs=-1, return_train_score=True,scoring='neg_mean_squared_error')
#grid_result = grid.fit(xpoly, ypoly)
##Graph lambda
#grid_result.cv_results_.keys()
#
#print(grid_result.cv_results_)
#
#
#logging.warning("Best: %f using %s" % (grid_result.best_score_, grid_result.best_params_))
#print("Best: %f using %s" % (grid_result.best_score_, grid_result.best_params_))
#means = grid_result.cv_results_['mean_test_score']
#stds = grid_result.cv_results_['std_test_score']
#params = grid_result.cv_results_['params']
#for mean, stdev, param in zip(means, stds, params):
#    print("%f (%f) with: %r" % (mean, stdev, param))
#
#print('DONE')
#print(grid_result.cv_results_['mean_test_score'])
#plt.plot(grid_result.cv_results_['mean_test_score'],'r',label = 'mean_test_score')
#plt.plot(grid_result.cv_results_['mean_train_score'],label = 'mean_train_score')
#plt.xlabel('features')
#plt.legend()
#plt.ylabel('Negative mean square error score')
#
#plt.title('Validation curve - lambda', fontsize = 18, y = 1.03)
#plt.tight_layout()
#plt.savefig('lambda.png')
#plt.show()
#print('End lambda')
#
#
### hledání nejlepšího počtu vrstev
#param_grid = dict(
#    n_layers=[2, 3, 4, 5], # počet vrstev
#    #first_layer_nodes=[240, 241],
#    #last_layer_nodes=[4],  #počet neuronů na poseldní vrstvě
#    #activation_func=activation_funcs, #aktivační funkce
#    #loss_func=loss_funcs,
#    #batch_size=[100, 200, 500, 1000, 2000, 5000], #
#    #nb_epoch=[20, 10, 30, 40, 50],
#    #l=l,
#    #n_inputs=[poly.n_output_features_]
#)
#
#grid = GridSearchCV(estimator=regressor, param_grid=param_grid, n_jobs=-1,
#                    return_train_score=True, scoring='neg_mean_squared_error')
#grid_result = grid.fit(xpoly, ypoly)
##Graph lambda
#grid_result.cv_results_.keys()
#
#print(grid_result.cv_results_)
#
#
#logging.warning("Best: %f using %s" % (grid_result.best_score_, grid_result.best_params_))
#print("Best: %f using %s" % (grid_result.best_score_, grid_result.best_params_))
#means = grid_result.cv_results_['mean_test_score']
#stds = grid_result.cv_results_['std_test_score']
#params = grid_result.cv_results_['params']
#for mean, stdev, param in zip(means, stds, params):
#    print("%f (%f) with: %r" % (mean, stdev, param))
#
#print('DONE')
#print(grid_result.cv_results_['mean_test_score'])
#plt.plot(grid_result.cv_results_[
#         'mean_test_score'], 'r', label='mean_test_score')
#plt.plot(grid_result.cv_results_['mean_train_score'], label='mean_train_score')
#plt.xlabel('features')
#plt.legend()
#plt.ylabel('Negative mean square error score')
#
#plt.title('Validation curve - layers', fontsize=18, y=1.03)
#plt.tight_layout()
#plt.savefig('layers.png')
#plt.show()
#print('End layers')
#
### hledání nejlepšího počtu neuronů na poslední vrstvě
#param_grid = dict(
#    #n_layers=[2, 3, 4, 5],  # počet vrstev
#    #first_layer_nodes=[240, 241],
#    last_layer_nodes=[2,3,4,5],  #počet neuronů na poseldní vrstvě
#    #activation_func=activation_funcs, #aktivační funkce
#    #loss_func=loss_funcs,
#    #batch_size=[100, 200, 500, 1000, 2000, 5000], #
#    #nb_epoch=[20, 10, 30, 40, 50],
#    #l=l,
#    #n_inputs=[poly.n_output_features_]
#)
#
#grid = GridSearchCV(estimator=regressor, param_grid=param_grid, n_jobs=-1,
#                    return_train_score=True, scoring='neg_mean_squared_error')
#grid_result = grid.fit(xpoly, ypoly)
##Graph lambda
#grid_result.cv_results_.keys()
#
#print(grid_result.cv_results_)
#
#
#logging.warning("Best: %f using %s" % (grid_result.best_score_, grid_result.best_params_))
#print("Best: %f using %s" % (grid_result.best_score_, grid_result.best_params_))
#means = grid_result.cv_results_['mean_test_score']
#stds = grid_result.cv_results_['std_test_score']
#params = grid_result.cv_results_['params']
#for mean, stdev, param in zip(means, stds, params):
#    print("%f (%f) with: %r" % (mean, stdev, param))
#
#print('DONE')
#print(grid_result.cv_results_['mean_test_score'])
#plt.plot(grid_result.cv_results_[
#         'mean_test_score'], 'r', label='mean_test_score')
#plt.plot(grid_result.cv_results_['mean_train_score'], label='mean_train_score')
#plt.xlabel('features')
#plt.legend()
#plt.ylabel('Negative mean square error score')
#
#plt.title('Validation curve - neurons on last layer', fontsize=18, y=1.03)
#plt.tight_layout()
#plt.savefig('neurons_last.png')
#plt.show()
#print('End neurons on last layer')
#print(poly.n_output_features_)
### hledání nejlepšího batch_size
#param_grid = dict(
#    #n_layers=[2, 3, 4, 5],  # počet vrstev
#    #first_layer_nodes=[240, 241],
#    #last_layer_nodes=[2, 3, 4, 5],  # počet neuronů na poseldní vrstvě
#    #activation_func=activation_funcs, #aktivační funkce
#    #loss_func=loss_funcs,
#    batch_size=[10, 20, 50, 100, 200, 500], #
#    #nb_epoch=[20, 10, 30, 40, 50],
#    #l=l,
#    #n_inputs=[poly.n_output_features_]
#)
#
#grid = GridSearchCV(estimator=regressor, param_grid=param_grid, n_jobs=-1,
#                    return_train_score=True, scoring='neg_mean_squared_error')
#grid_result = grid.fit(xpoly, ypoly)
##Graph lambda
#grid_result.cv_results_.keys()
#
#print(grid_result.cv_results_)
#
#
#logging.warning("Best: %f using %s" % (grid_result.best_score_, grid_result.best_params_))
#print("Best: %f using %s" % (grid_result.best_score_, grid_result.best_params_))
#means = grid_result.cv_results_['mean_test_score']
#stds = grid_result.cv_results_['std_test_score']
#params = grid_result.cv_results_['params']
#for mean, stdev, param in zip(means, stds, params):
#    print("%f (%f) with: %r" % (mean, stdev, param))
#
#print('DONE')
#print(grid_result.cv_results_['mean_test_score'])
#plt.plot(grid_result.cv_results_[
#         'mean_test_score'], 'r', label='mean_test_score')
#plt.plot(grid_result.cv_results_['mean_train_score'], label='mean_train_score')
#plt.xlabel('features')
#plt.legend()
#plt.ylabel('Negative mean square error score')
#
#plt.title('Validation curve - batch size', fontsize=18, y=1.03)
#plt.tight_layout()
#plt.savefig('batch_size.png')
#plt.show()
#print('End batch size')
##
##
##file = open('parameters_tuning.txt', 'wb')
##pickle.dump(grid_result.cv_results_, file)
##file_best = open('best_parameters.txt', 'w')
##pickle.dump("Best: %f using %s" % (grid_result.best_score_, grid_result.best_params_), file_best)
##file.close()
##file_best.close()
