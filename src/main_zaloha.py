from matplotlib import pyplot as plt
from sklearn.tree import DecisionTreeRegressor
from sklearn.metrics import r2_score
from sklearn.metrics import classification_report
from sklearn import metrics
import warnings
import lime.lime_tabular
import lime
import eli5
from sklearn.utils import resample
from sklearn.kernel_ridge import KernelRidge
from sklearn.svm import SVR
from sklearn.ensemble import RandomForestRegressor, GradientBoostingRegressor
from sklearn.gaussian_process import GaussianProcessRegressor
from xgboost import XGBRegressor
from sklearn.metrics import mean_squared_error, r2_score
from sklearn.linear_model import Ridge, RidgeCV, Lasso, LassoCV, LinearRegression, ElasticNet,  HuberRegressor
from sklearn.model_selection import train_test_split, cross_val_score, GridSearchCV, KFold, StratifiedKFold, RandomizedSearchCV
from sklearn.compose import ColumnTransformer
from sklearn.pipeline import Pipeline
from sklearn.preprocessing import scale, StandardScaler, RobustScaler, OneHotEncoder, PolynomialFeatures
import statsmodels.api as sm
import statsmodels
from scipy.special import boxcox1p
from scipy.stats import norm
import scipy.stats as stats
import itertools
import collections
from plotly.subplots import make_subplots
import plotly.graph_objs as go
import plotly as py
from plotly.offline import download_plotlyjs, init_notebook_mode, plot, iplot
import numpy as np  # linear algebra
import pandas as pd  # data processing, CSV file I/O (e.g. pd.read_csv)
import matplotlib
import matplotlib.pyplot as plt
import seaborn as sns
from keras.models import Sequential
from keras.layers import Dense
from keras.wrappers.scikit_learn import KerasRegressor
from sklearn.model_selection import cross_val_score
from sklearn.model_selection import KFold
from sklearn.preprocessing import StandardScaler
from sklearn.pipeline import Pipeline
plt.style.use('fivethirtyeight')
matplotlib.rcParams['font.family'] = "Arial"


init_notebook_mode(connected=True)


#print(statsmodels.__version__)


#Model interpretation modules
#import shap
#shap.initjs()

warnings.filterwarnings("ignore", category=FutureWarning)

#Load Data
Combined_data = pd.read_csv('../AB_NYC_2019.csv')
Combined_data.head()
print('Number of features: {}'.format(Combined_data.shape[1]))
print('Number of examples: {}'.format(Combined_data.shape[0]))
Combined_data.dtypes
Combined_data['last_review'] = pd.to_datetime(
	Combined_data['last_review'], infer_datetime_format=True)

#Missing data
total = Combined_data.isnull().sum().sort_values(ascending=False)
percent = (Combined_data.isnull().sum()) / \
    Combined_data.isnull().count().sort_values(ascending=False)
missing_data = pd.concat([total, percent], axis=1, keys=[
                         'Total', 'Percent'], sort=False).sort_values('Total', ascending=False)
missing_data.head(40)
Combined_data.drop(['host_name', 'name'], axis=1, inplace=True)
Combined_data[Combined_data['number_of_reviews'] == 0.0].shape
Combined_data['reviews_per_month'] = Combined_data['reviews_per_month'].fillna(
    0)
earliest = min(Combined_data['last_review'])
Combined_data['last_review'] = Combined_data['last_review'].fillna(earliest)
Combined_data['last_review'] = Combined_data['last_review'].apply(
    lambda x: x.toordinal() - earliest.toordinal())
total = Combined_data.isnull().sum().sort_values(ascending=False)
percent = (Combined_data.isnull().sum()) / \
    Combined_data.isnull().count().sort_values(ascending=False)
missing_data = pd.concat([total, percent], axis=1, keys=[
                         'Total', 'Percent'], sort=False).sort_values('Total', ascending=False)
missing_data.head(40)

#Price distribution
fig, axes = plt.subplots(1, 3, figsize=(21, 6))
sns.distplot(Combined_data['price'], ax=axes[0])
sns.distplot(np.log1p(Combined_data['price']), ax=axes[1])
axes[1].set_xlabel('log(1+price)')
sm.qqplot(np.log1p(Combined_data['price']),
          stats.norm, fit=True, line='45', ax=axes[2])
Combined_data = Combined_data[np.log1p(Combined_data['price']) < 8]
Combined_data = Combined_data[np.log1p(Combined_data['price']) > 3]
fig, axes = plt.subplots(1, 3, figsize=(21, 6))
sns.distplot(Combined_data['price'], ax=axes[0])
sns.distplot(np.log1p(Combined_data['price']), ax=axes[1])
axes[1].set_xlabel('log(1+price)')
sm.qqplot(np.log1p(Combined_data['price']),
          stats.norm, fit=True, line='45', ax=axes[2])
Combined_data['price'] = np.log1p(Combined_data['price'])

#Predictor distributions
print(Combined_data.columns)
print('In this dataset there are {} unique hosts renting out  a total number of {} properties.'.format(
    len(Combined_data['host_id'].unique()), Combined_data.shape[0]))
Combined_data = Combined_data.drop(['host_id', 'id'], axis=1)

#neighbourhood group
sns.catplot(x='neighbourhood_group', kind='count', data=Combined_data)
fig = plt.gcf()
fig.set_size_inches(12, 6)


#Longitude and Latitude
fig, axes = plt.subplots(1, 3, figsize=(21, 6))
sns.distplot(Combined_data['latitude'], ax=axes[0])
sns.distplot(Combined_data['longitude'], ax=axes[1])
sns.scatterplot(x=Combined_data['latitude'], y=Combined_data['longitude'])

#Room type
sns.catplot(x='room_type', kind='count', data=Combined_data)
fig = plt.gcf()
fig.set_size_inches(8, 6)

#Minimum nights
fig, axes = plt.subplots(1, 2, figsize=(21, 6))

sns.distplot(Combined_data['minimum_nights'],
             rug=False, kde=False, color="green", ax=axes[0])
axes[0].set_yscale('log')
axes[0].set_xlabel('minimum stay [nights]')
axes[0].set_ylabel('count')

sns.distplot(np.log1p(Combined_data['minimum_nights']),
             rug=False, kde=False, color="green", ax=axes[1])
axes[1].set_yscale('log')
axes[1].set_xlabel('minimum stay [nights]')
axes[1].set_ylabel('count')

#Reviews per month
fig, axes = plt.subplots(1, 2, figsize=(18.5, 6))
sns.distplot(Combined_data[Combined_data['reviews_per_month'] < 17.5]
             ['reviews_per_month'], rug=True, kde=False, color="green", ax=axes[0])
sns.distplot(np.sqrt(Combined_data[Combined_data['reviews_per_month'] < 17.5]
                     ['reviews_per_month']), rug=True, kde=False, color="green", ax=axes[1])
axes[1].set_xlabel('ln(reviews_per_month)')
fig, axes = plt.subplots(1, 1, figsize=(21, 6))
sns.scatterplot(x=Combined_data['availability_365'],
                y=Combined_data['reviews_per_month'])
Combined_data['reviews_per_month'] = Combined_data[Combined_data['reviews_per_month']
                                                   < 17.5]['reviews_per_month']

#Availability 365
fig, axes = plt.subplots(1, 1, figsize=(18.5, 6))
sns.distplot(Combined_data['availability_365'],
             rug=False, kde=False, color="blue", ax=axes)
axes.set_xlabel('availability_365')
axes.set_xlim(0, 365)


#Feature engineering
Combined_data['all_year_avail'] = Combined_data['availability_365'] > 353
Combined_data['low_avail'] = Combined_data['availability_365'] < 12
Combined_data['no_reviews'] = Combined_data['reviews_per_month'] == 0

#pearson corelation matrix
# corrmatrix = Combined_data.corr()
corrmatrix = Combined_data.corr(method='kendall')
f, ax = plt.subplots(figsize=(15, 12))
sns.heatmap(corrmatrix, vmax=0.8, square=True)
sns.set(font_scale=0.8)
plt.show()


#Encoding categorical features
# najde datatypy v dataframu, kteří jsou typu objekt
categorical_features = Combined_data.select_dtypes(include=['object'])
print('Categorical features: {}'.format(categorical_features.shape))
# automaticky vytvoří kategorický data (např. část města) na číselné hodnoty
categorical_features_one_hot = pd.get_dummies(categorical_features)
categorical_features_one_hot.head()
Combined_data['reviews_per_month'] = Combined_data['reviews_per_month'].fillna(
	0)

#Save transformed dataframe for future use
# vyberou se z původního datasetu číselné hodnoty
numerical_features = Combined_data.select_dtypes(exclude=['object'])
print(Combined_data.dtypes)
y = numerical_features.price  # price jako target
numerical_features = numerical_features.drop(
	['price'], axis=1)  # vyloučení price z features
print('Numerical features: {}'.format(numerical_features.shape))
X = np.concatenate((numerical_features, categorical_features_one_hot), axis=1)
# zkombinování původních číselných dat a kategorických převedených dat
X_df = pd.concat([numerical_features, categorical_features_one_hot], axis=1)
print(X_df.head())

#remove duplicates
print('--------------------------')
print(X_df.describe())
#print('Dimensions of the design matrix: {}'.format(X.shape))
#print('Dimension of the target vector: {}'.format(y.shape))


#Train-test split
X_train, X_test, y_train, y_test = train_test_split(
    X_df, y, test_size=0.2, random_state=42)
print('Dimensions of the training feature matrix: {}'.format(X_train.shape))
print('Dimensions of the training target vector: {}'.format(y_train.shape))
print('Dimensions of the test feature matrix: {}'.format(X_test.shape))
print('Dimensions of the test target vector: {}'.format(y_test.shape))

#Fitting the Multiple Linear Regression model
mlr = LinearRegression()
mlr.fit(X_train, y_train)

# Intercept and Coefficient
print("Intercept: ", mlr.intercept_)
print("Coefficients:")
print(list(zip(X_df, mlr.coef_)))


#Prediction of test set
y_pred_mlr = mlr.predict(X_test)
y_test = np.expm1(y_test)
y_pred_mlr = np.expm1(y_pred_mlr)
#Predicted values
print("Prediction for test set: {}".format(y_pred_mlr))
#Actual value and the predicted value
mlr_diff = pd.DataFrame(
    {'Actual value': y_test, 'Predicted value': y_pred_mlr})
print(mlr_diff.head(40))

#Model Evaluation
r2score = r2_score(y_test, y_pred_mlr)
meanAbErr = metrics.mean_absolute_error(y_test, y_pred_mlr)
meanSqErr = metrics.mean_squared_error(y_test, y_pred_mlr)
maxErr = metrics.max_error(y_test, y_pred_mlr)
rootMeanSqErr = np.sqrt(metrics.mean_squared_error(y_test, y_pred_mlr))
print('R2 score:', r2score)
#print('R squared: {:.2f}'.format(mlr.score(X_df, y)*100))
print('Mean Absolute Error:', meanAbErr)
print('Mean Square Error:', meanSqErr)
print('Root Mean Square Error:', rootMeanSqErr)
print('Max Error:', maxErr)
print('-------------------------------------------')
#-------------------------NN-+-------------------------
# define base model


def baseline_model():
	# create model
	model = Sequential()
	# přidávání skrytých, vstupních a výstupních vrstev | relu je udajně nejlepší, ale další je třeba sigmoid - asi vyzkoušet
	model.add(Dense(241, input_dim=241,
                 kernel_initializer='normal', activation='relu'))
	model.add(Dense(1, kernel_initializer='normal'))
	# Compile model
	model.compile(loss='mean_squared_error', optimizer='adam')
	return model


# train first NN

poly = PolynomialFeatures(1)
ypoly = y_train
xpoly = poly.fit_transform(X_train)
ypoly = np.asarray(ypoly).astype(np.float32)
xpoly = np.asarray(xpoly).astype(np.float32)
model = baseline_model()
history = model.fit(xpoly, ypoly, epochs=5,
                    batch_size=10, validation_split=0.3)


plt.plot(history.history['loss'], label='train loss')
plt.plot(history.history['val_loss'], label='validation loss')
plt.ylabel('Loss')
plt.xlabel('Number of epochs')
plt.title('Learning curve - n. epochs', fontsize=18, y=1.03)
plt.legend()
plt.savefig('NN_learning curve.png')
plt.show()

#train optimized
poly = PolynomialFeatures(1)
ypoly = y_train
xpoly = poly.fit_transform(X_train)
model = baseline_model()

regressor = KerasRegressor(build_fn=baseline_model,
                           epochs=5, batch_size=10, verbose=0)
print(regressor.get_params())
param_grid = {'batch_size': [100, 20, 50, 25, 32],
              'nb_epoch': [200, 100, 300, 400]
              }
grid = GridSearchCV(estimator=regressor, param_grid=param_grid, n_jobs=-1,
                    return_train_score=True, scoring='neg_mean_squared_error')
grid_result = grid.fit(xpoly, ypoly)
#Graph labda
grid_result.cv_results_.keys()

print(grid_result.cv_results_)
print("Best: %f using %s" % (grid_result.best_score_, grid_result.best_params_))
means = grid_result.cv_results_['mean_test_score']
stds = grid_result.cv_results_['std_test_score']
params = grid_result.cv_results_['params']
for mean, stdev, param in zip(means, stds, params):
    print("%f (%f) with: %r" % (mean, stdev, param))

print('DONE')
print(grid_result.cv_results_['mean_test_score'])
plt.plot(grid_result.cv_results_[
         'mean_test_score'], 'r', label='mean_test_score')
plt.plot(grid_result.cv_results_['mean_train_score'], label='mean_train_score')
plt.xlabel('lambda')
plt.legend()
plt.ylabel('Negative mean square error score')

plt.title('Validation curve - lambda', fontsize=18, y=1.03)
plt.savefig('lambda.png')
plt.show()
print('End')

#tohle nějak nefunguje a nejsme jediní místo tohoto použítí nad
## evaluate model with standardized dataset
#estimator = KerasRegressor(build_fn=baseline_model,nb_epoch=100, batch_size=5, verbose=0)
#
#kfold = KFold(n_splits=10, shuffle = True, random_state=seed)
#results = cross_val_score(estimator, X, y, cv=kfold, n_jobs=1)
#print("Results: %.2f (%.2f) MSE" % (results.mean(), results.std()))

# evaluate model with standardized dataset
#np.random.seed(seed)
#estimators = []
#estimators.append(('standardize', StandardScaler()))
#estimators.append(('mlp', KerasRegressor(
#	build_fn=baseline_model, epochs=50, batch_size=5, verbose=0)))
#pipeline = Pipeline(estimators)
#kfold = KFold(n_splits=10, shuffle = True, random_state=seed)
#results = cross_val_score(pipeline, X, y, cv=kfold, n_jobs=1)
#print("Standardized: %.2f (%.2f) MSE" % (results.mean(), results.std()))
#
## define the model
#
#
#def larger_model():
#	# create model
#	model = Sequential()
#	model.add(Dense(13, input_dim=13, kernel_initializer='normal', activation='relu'))
#	model.add(Dense(6, kernel_initializer='normal', activation='relu'))
#	model.add(Dense(1, kernel_initializer='normal'))
#	# Compile model
#	model.compile(loss='mean_squared_error', optimizer='adam')
#	return model
#
#
#np.random.seed(seed)
#estimators = []
#estimators.append(('standardize', StandardScaler()))
#estimators.append(('mlp', KerasRegressor(
#	build_fn=larger_model, epochs=50, batch_size=5, verbose=0)))
#pipeline = Pipeline(estimators)
#kfold = KFold(n_splits=10, shuffle = True, random_state=seed)
#results = cross_val_score(pipeline, X, Y, cv=kfold, n_jobs=1)
#print("Larger: %.2f (%.2f) MSE" % (results.mean(), results.std()))
#
## define wider model
#def wider_model():
#	# create model
#	model = Sequential()
#	model.add(Dense(20, input_dim=13, kernel_initializer='normal', activation='relu'))
#	model.add(Dense(1, kernel_initializer='normal'))
#	# Compile model
#	model.compile(loss='mean_squared_error', optimizer='adam')
#	return model
#
#
#np.random.seed(seed)
#estimators = []
#estimators.append(('standardize', StandardScaler()))
#estimators.append(('mlp', KerasRegressor(
#	build_fn=wider_model, epochs=100, batch_size=5, verbose=0)))
#pipeline = Pipeline(estimators)
#kfold = KFold(n_splits=10, shuffle = True, random_state=seed)
#results = cross_val_score(pipeline, X, Y, cv=kfold, n_jobs=1)
#print("Wider: %.2f (%.2f) MSE" % (results.mean(), results.std()))
#-------------------------NN-+-------------------------
##Decision Tree Regression
#x_train, x_test, y_train, y_test = train_test_split(
#    X_df, y, test_size=.1, random_state=105)
#DTree = DecisionTreeRegressor(min_samples_leaf=.0001)
#DTree.fit(x_train, y_train)
#y_predict_dtr = DTree.predict(x_test)
#
##Model Evaluation
#y_test = np.expm1(y_test)
#y_predict_dtr = np.expm1(y_predict_dtr)
#
#
#r2score_dtr = r2_score(y_test, y_predict_dtr)
#meanAbErr_dtr = metrics.mean_absolute_error(y_test, y_predict_dtr)
#meanSqErr_dtr = metrics.mean_squared_error(y_test, y_predict_dtr)
#maxErr_dtr = metrics.max_error(y_test, y_predict_dtr)
#rootMeanSqErr_dtr = np.sqrt(metrics.mean_squared_error(y_test, y_predict_dtr))
#print('R2 score:', r2score_dtr)
##print('R squared: {:.2f}'.format(mlr.score(X_df, y)*100))
#print('Mean Absolute Error:', meanAbErr_dtr)
#print('Mean Square Error:', meanSqErr_dtr)
#print('Root Mean Square Error:', rootMeanSqErr_dtr)
#print('Max Error:', maxErr_dtr)
#print('-------------------------------------------')
#
##Lasso regression
#x_train, x_test, y_train, y_test = train_test_split(
#    X_df, y, test_size=.1, random_state=105)
#ls = Lasso(alpha=0.001)
#ls.fit(x_train, y_train)
#y_predict_ls = ls.predict(x_test)
#
##Model Evaluation
#y_test = np.expm1(y_test)
#y_predict_ls = np.expm1(y_predict_ls)
#
#
#r2score_ls = r2_score(y_test, y_predict_ls)
#meanAbErr_ls = metrics.mean_absolute_error(y_test, y_predict_ls)
#meanSqErr_ls = metrics.mean_squared_error(y_test, y_predict_ls)
#maxErr_ls = metrics.max_error(y_test, y_predict_ls)
#rootMeanSqErr_ls = np.sqrt(metrics.mean_squared_error(y_test, y_predict_ls))
#print('R2 score:', r2score_ls)
##print('R squared: {:.2f}'.format(mlr.score(X_df, y)*100))
#print('Mean Absolute Error:', meanAbErr_ls)
#print('Mean Square Error:', meanSqErr_ls)
#print('Root Mean Square Error:', rootMeanSqErr_ls)
#print('Max Error:', maxErr_ls)
##Rescale the design matrix
#scaler = RobustScaler()
#X_train = scaler.fit_transform(X_train)
#X_test = scaler.fit_transform(X_test)
##cross-validation routine
#n_folds = 5
#
## squared_loss
#
#
#def rmse_cv(model):
#    kf = KFold(n_folds, shuffle=True, random_state=91).get_n_splits(
#        numerical_features)
#    return cross_val_score(model, X_train, y_train, scoring='neg_mean_squared_error', cv=kf)
#
#
#def rmse_lv_cv(model):
#    kf = KFold(n_folds, shuffle=True, random_state=91).get_n_splits(
#        numerical_features)
#    return cross_val_score(model, Xlv_train, y_train, scoring='neg_mean_squared_error', cv=kf)
#
#
## Scoring basic models (no parameter tuning)
##for Model in [LinearRegression, Ridge, Lasso, ElasticNet, RandomForestRegressor, XGBRegressor, HuberRegressor]:
##    if Model == XGBRegressor:
##        cv_res = rmse_cv(XGBRegressor(objective='reg:squarederror'))
##    elif Model == HuberRegressor:
##        cv_res = rmse_cv(HuberRegressor(max_iter=500))
##    else:
##        cv_res = rmse_cv(Model())
##    print('{}: {:.5f} +/- {:5f}'.format(Model.__name__, -cv_res.mean(), cv_res.std()))
##
#cv_res = rmse_cv(LinearRegression())
#print('{}: {:.5f} +/- {:5f}'.format(LinearRegression.__name__, -
#                                    cv_res.mean(), cv_res.std()))
#cv_res = rmse_cv(Lasso())
#print('{}: {:.5f} +/- {:5f}'.format(Lasso.__name__, -cv_res.mean(), cv_res.std()))
#cv_res = rmse_cv(Ridge())
#print('{}: {:.5f} +/- {:5f}'.format(Ridge.__name__, -cv_res.mean(), cv_res.std()))
