import warnings
import lime.lime_tabular
import lime
import eli5
from sklearn.utils import resample
from sklearn.kernel_ridge import KernelRidge
from sklearn.svm import SVR
from sklearn.ensemble import RandomForestRegressor, GradientBoostingRegressor
from sklearn.gaussian_process import GaussianProcessRegressor
from xgboost import XGBRegressor
from sklearn.metrics import mean_squared_error, r2_score
from sklearn.linear_model import Ridge, RidgeCV, Lasso, LassoCV, LinearRegression, ElasticNet,  HuberRegressor
from sklearn.model_selection import train_test_split, cross_val_score, GridSearchCV, KFold, StratifiedKFold, RandomizedSearchCV
from sklearn.compose import ColumnTransformer
from sklearn.pipeline import Pipeline
from sklearn.preprocessing import scale, StandardScaler, RobustScaler, OneHotEncoder
import statsmodels.api as sm
import statsmodels
from scipy.special import boxcox1p
from scipy.stats import norm
import scipy.stats as stats
import itertools
import collections
from plotly.subplots import make_subplots
import plotly.graph_objs as go
import plotly as py
from plotly.offline import download_plotlyjs, init_notebook_mode, plot, iplot
import numpy as np  # linear algebra
import pandas as pd  # data processing, CSV file I/O (e.g. pd.read_csv)
import matplotlib
import matplotlib.pyplot as plt
import seaborn as sns
plt.style.use('fivethirtyeight')
matplotlib.rcParams['font.family'] = "Arial"


init_notebook_mode(connected=True)


#print(statsmodels.__version__)


#Model interpretation modules
#import shap
#shap.initjs()

warnings.filterwarnings("ignore", category=FutureWarning)
#Load Data
Combined_data = pd.read_csv('../AB_NYC_2019.csv')
Combined_data.head()
print('Number of features: {}'.format(Combined_data.shape[1]))
print('Number of examples: {}'.format(Combined_data.shape[0]))
Combined_data.dtypes
Combined_data['last_review'] = pd.to_datetime(
    Combined_data['last_review'], infer_datetime_format=True)
#Missing data
total = Combined_data.isnull().sum().sort_values(ascending=False)
percent = (Combined_data.isnull().sum()) / \
    Combined_data.isnull().count().sort_values(ascending=False)
missing_data = pd.concat([total, percent], axis=1, keys=[
                         'Total', 'Percent'], sort=False).sort_values('Total', ascending=False)
missing_data.head(40)
Combined_data.drop(['host_name', 'name'], axis=1, inplace=True)
Combined_data[Combined_data['number_of_reviews'] == 0.0].shape
Combined_data['reviews_per_month'] = Combined_data['reviews_per_month'].fillna(0)
earliest = min(Combined_data['last_review'])
Combined_data['last_review'] = Combined_data['last_review'].fillna(earliest)
Combined_data['last_review'] = Combined_data['last_review'].apply(
    lambda x: x.toordinal() - earliest.toordinal())
total = Combined_data.isnull().sum().sort_values(ascending=False)
percent = (Combined_data.isnull().sum()) / \
    Combined_data.isnull().count().sort_values(ascending=False)
missing_data = pd.concat([total, percent], axis=1, keys=[
                         'Total', 'Percent'], sort=False).sort_values('Total', ascending=False)
missing_data.head(40)
#Price distribution
fig, axes = plt.subplots(1, 3, figsize=(21, 6))
sns.distplot(Combined_data['price'], ax=axes[0])
sns.distplot(np.log1p(Combined_data['price']), ax=axes[1])
axes[1].set_xlabel('log(1+price)')
sm.qqplot(np.log1p(Combined_data['price']),
          stats.norm, fit=True, line='45', ax=axes[2])
Combined_data = Combined_data[np.log1p(Combined_data['price']) < 8]
Combined_data = Combined_data[np.log1p(Combined_data['price']) > 3]
fig, axes = plt.subplots(1, 3, figsize=(21, 6))
sns.distplot(Combined_data['price'], ax=axes[0])
sns.distplot(np.log1p(Combined_data['price']), ax=axes[1])
axes[1].set_xlabel('log(1+price)')
sm.qqplot(np.log1p(Combined_data['price']),
          stats.norm, fit=True, line='45', ax=axes[2])
Combined_data['price'] = np.log1p(Combined_data['price'])
#Predictor distributions
print(Combined_data.columns)
print('In this dataset there are {} unique hosts renting out  a total number of {} properties.'.format(
    len(Combined_data['host_id'].unique()), Combined_data.shape[0]))
Combined_data = Combined_data.drop(['host_id', 'id'], axis=1)

#neighbourhood group
sns.catplot(x='neighbourhood_group', kind='count', data=Combined_data)
fig = plt.gcf()
fig.set_size_inches(12, 6)


#Longitude and Latitude
fig, axes = plt.subplots(1, 3, figsize=(21, 6))
sns.distplot(Combined_data['latitude'], ax=axes[0])
sns.distplot(Combined_data['longitude'], ax=axes[1])
sns.scatterplot(x=Combined_data['latitude'], y=Combined_data['longitude'])
#Room type
sns.catplot(x='room_type', kind='count', data=Combined_data)
fig = plt.gcf()
fig.set_size_inches(8, 6)

#Minimum nights
fig, axes = plt.subplots(1, 2, figsize=(21, 6))

sns.distplot(Combined_data['minimum_nights'],
             rug=False, kde=False, color="green", ax=axes[0])
axes[0].set_yscale('log')
axes[0].set_xlabel('minimum stay [nights]')
axes[0].set_ylabel('count')

sns.distplot(np.log1p(Combined_data['minimum_nights']),
             rug=False, kde=False, color="green", ax=axes[1])
axes[1].set_yscale('log')
axes[1].set_xlabel('minimum stay [nights]')
axes[1].set_ylabel('count')
#Reviews per month
fig, axes = plt.subplots(1, 2, figsize=(18.5, 6))
sns.distplot(Combined_data[Combined_data['reviews_per_month'] < 17.5]
             ['reviews_per_month'], rug=True, kde=False, color="green", ax=axes[0])
sns.distplot(np.sqrt(Combined_data[Combined_data['reviews_per_month'] < 17.5]
                     ['reviews_per_month']), rug=True, kde=False, color="green", ax=axes[1])
axes[1].set_xlabel('ln(reviews_per_month)')
fig, axes = plt.subplots(1, 1, figsize=(21, 6))
sns.scatterplot(x=Combined_data['availability_365'],
                y=Combined_data['reviews_per_month'])
Combined_data['reviews_per_month'] = Combined_data[Combined_data['reviews_per_month']
                                                   < 17.5]['reviews_per_month']
#Availability 365
fig, axes = plt.subplots(1, 1, figsize=(18.5, 6))
sns.distplot(Combined_data['availability_365'],
             rug=False, kde=False, color="blue", ax=axes)
axes.set_xlabel('availability_365')
axes.set_xlim(0, 365)

#Feature engineering

Combined_data['all_year_avail'] = Combined_data['availability_365'] > 353
Combined_data['low_avail'] = Combined_data['availability_365'] < 12
Combined_data['no_reviews'] = Combined_data['reviews_per_month'] == 0

#pearson corelation matrix
corrmatrix = Combined_data.corr()
f, ax = plt.subplots(figsize=(15, 12))
sns.heatmap(corrmatrix, vmax=0.8, square=True)
sns.set(font_scale=0.8)
#Encoding categorical features
categorical_features = Combined_data.select_dtypes(include=['object'])
print('Categorical features: {}'.format(categorical_features.shape))
categorical_features_one_hot = pd.get_dummies(categorical_features)
categorical_features_one_hot.head()
Combined_data['reviews_per_month'] = Combined_data['reviews_per_month'].fillna(
    0)
#Save transformed dataframe for future use
numerical_features = Combined_data.select_dtypes(exclude=['object'])
y = numerical_features.price
numerical_features = numerical_features.drop(['price'], axis=1)
print('Numerical features: {}'.format(numerical_features.shape))
X = np.concatenate((numerical_features, categorical_features_one_hot), axis=1)
X_df = pd.concat([numerical_features, categorical_features_one_hot], axis=1)
#print('Dimensions of the design matrix: {}'.format(X.shape))
#print('Dimension of the target vector: {}'.format(y.shape))
#Train-test split
X_train, X_test, y_train, y_test = train_test_split(
    X, y, test_size=0.2, random_state=42)
print('Dimensions of the training feature matrix: {}'.format(X_train.shape))
print('Dimensions of the training target vector: {}'.format(y_train.shape))
print('Dimensions of the test feature matrix: {}'.format(X_test.shape))
print('Dimensions of the test target vector: {}'.format(y_test.shape))
#Rescale the design matrix
scaler = RobustScaler()
X_train = scaler.fit_transform(X_train)
X_test = scaler.fit_transform(X_test)
#cross-validation routine
n_folds = 5

# squared_loss


def rmse_cv(model):
    kf = KFold(n_folds, shuffle=True, random_state=91).get_n_splits(
        numerical_features)
    return cross_val_score(model, X_train, y_train, scoring='neg_mean_squared_error', cv=kf)


def rmse_lv_cv(model):
    kf = KFold(n_folds, shuffle=True, random_state=91).get_n_splits(
        numerical_features)
    return cross_val_score(model, Xlv_train, y_train, scoring='neg_mean_squared_error', cv=kf)


# Scoring basic models (no parameter tuning)
#for Model in [LinearRegression, Ridge, Lasso, ElasticNet, RandomForestRegressor, XGBRegressor, HuberRegressor]:
#    if Model == XGBRegressor:
#        cv_res = rmse_cv(XGBRegressor(objective='reg:squarederror'))
#    elif Model == HuberRegressor:
#        cv_res = rmse_cv(HuberRegressor(max_iter=500))
#    else:
#        cv_res = rmse_cv(Model())
#    print('{}: {:.5f} +/- {:5f}'.format(Model.__name__, -cv_res.mean(), cv_res.std()))
#
cv_res = rmse_cv(LinearRegression())
print('{}: {:.5f} +/- {:5f}'.format(LinearRegression.__name__, -cv_res.mean(), cv_res.std()))
cv_res = rmse_cv(Lasso())
print('{}: {:.5f} +/- {:5f}'.format(Lasso.__name__, -cv_res.mean(), cv_res.std()))
cv_res = rmse_cv(Ridge())
print('{}: {:.5f} +/- {:5f}'.format(Ridge.__name__, -cv_res.mean(), cv_res.std()))
